const mongoose = require('mongoose');

let TipoSchema = new mongoose.Schema({
    nombre: {
        type: String
    }
});

let Tipo = mongoose.model('Tipo', TipoSchema);
module.exports = Tipo;