const mongoose = require('mongoose');

let InmuebleSchema = new mongoose.Schema({
    descripcion: {
        type: String,
        required: true,
        minlength: 10
    },
    tipo: {
        type: mongoose.Schema.ObjectId,
        ref: 'Tipo',
        required: true
    },
    numero_habitaciones: {
        type: Number,
        required: true,
        min: 1
    },
    superficie: {
        type: Number,
        min: 25
    },
    precio: {
        type: Number,
        required: true,
        min: 10000
    },
    imagen: {
        type: String,
    }
});

let Inmueble = mongoose.model('Inmueble', InmuebleSchema);
module.exports = Inmueble;