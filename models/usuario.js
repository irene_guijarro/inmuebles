const mongoose = require('mongoose');

let usuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    login: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    }
});

let Usuario = mongoose.model('usuario', usuarioSchema);

module.exports = Usuario;