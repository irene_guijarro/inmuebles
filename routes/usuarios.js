const express = require('express');
const Usuario = require('../models/usuario');
const passport = require('passport');
const {Strategy, ExtractJwt} = require('passport-jwt');
const jwt = require('jsonwebtoken');
const md5 = require('md5');
const secreto = "secretoDAW";

let router = express.Router();

let generarToken = id => {
    return jwt.sign({id: id}, secreto, {expiresIn: "2 hours"});
}

passport.use(
    new Strategy(
      {
        secretOrKey: secreto,
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken()
      },
      (payload, done) => {
        if (payload.id) {
          return done(null, { id: payload.id });
        } else {
          return done(new Error("Usuario incorrecto"), null);
        }
      }
    )
);

router.post("/login", (req, res) => {
    Usuario.findOne({ login: req.body.login, password: md5(req.body.password) })
    .then(resultado => {
        if (resultado) res.send({ ok: true, token: generarToken(resultado._id) });
        else res.send({ ok: false, errorMessage: "Usuario incorrecto" });
    })
    .catch(error => {
        res.send({ ok: false, errorMessage: "Usuario incorrecto" });
    });
});

router.post('/registro', (req, res) => {
    let nuevoUsuario = {
        nombre: req.body.nombre,
        login: req.body.login,
        password: md5(req.body.password)
    };
    Usuario.findOne({login: req.body.login}).then(resultado => {
        if (resultado)
            res.send({ok: false, errorMessage: 'El usuario ya está registrado'});
        else {
            Usuario.create(nuevoUsuario).then(resultado => {
                res.redirect('login');
            }).catch(error => {
                res.render('registro', {ok: false, errorMessage: error});
            });
        }
    });
});

module.exports = router;