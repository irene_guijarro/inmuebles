const express = require('express');
const passport = require('passport');


let router = express.Router();

router.get('/', (req, res) => {
    res.render('index');
});

router.get('/nuevo_inmueble', passport.authenticate("jwt", {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    res.render('nuevo_inmueble');
});

router.get('/registro', (req, res) => {
    res.render('registro');
});

router.get('/login', (req, res) => {
    res.render('login');
});

router.get('/prohibido', (req, res) => {
    res.render('prohibido');
});

router.delete('/prohibido', (req, res) => {
    res.render('prohibido');
});
module.exports = router;