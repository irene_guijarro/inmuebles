const express = require('express');
const fs = require('fs');
const Inmueble = require('../models/inmueble');
const Tipo = require('../models/tipo');
const passport = require('passport');

let router = express.Router();

router.get('/', (req, res) => {
    Inmueble.find().populate('tipo').then(resultado => {
        res.render('listar_inmuebles', {ok: true, inmuebles: resultado});
    }).catch(error => {
        res.render('listar_inmuebles', {ok: false, errorMessage: error});
    });
});

router.get('/:id', (req, res) => {
    let id = req.params.id;
    Inmueble.findById(id).populate('tipo').then(resultado => {
        res.render('ficha_inmueble', {inmueble: resultado});
    }).catch(error => {
        res.render('ficha_inmueble', {inmueble: ''})
    })
});

router.get('/precio/superficie/habitaciones', (req, res) => {
    Inmueble.find().where('precio').lt(req.query.precio)
        .where('superficie').gt(req.query.superficie)
        .where('numero_habitaciones').gt(req.query.habitaciones)
        .populate('tipo').then(resultado => {
            res.render('listar_inmuebles', {ok: true, inmuebles: resultado});
    });
});

router.get('/tipo/:id', (req, res) => {
    let idTipo = req.params.id;
    Inmueble.find({tipo: idTipo}).populate('tipo').then(resultado => {
        res.render('listar_inmuebles', {ok: true, inmuebles: resultado});
    });
})

router.post('/', passport.authenticate("jwt", {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    let nombreTipo = req.body.tipo;
    let nuevoInmueble = req.body;
    if (req.files.imagen) {
        req.files.imagen.mv('./public/imgs/' + req.files.imagen.name, (err) => {
            if (err) {
                return res.status(500).send("Error subiendo fichero");
            }
            nuevoInmueble.imagen = req.files.imagen.name;
            Tipo.findOne({nombre: nombreTipo}).then(tipoInmueble => {
                nuevoInmueble.tipo = tipoInmueble.id;
                if (esValidoInmueble(nuevoInmueble)) {
                    let inmuebleModelo = new Inmueble(nuevoInmueble);
                    inmuebleModelo.save().then(resultado => {
                        res.send({ok: true});
                    }).catch(error => {
                        res.render('nuevo_inmueble', {ok: false, errorMessage: error});
                    })
                }
            }); 
        });
    }   
});

router.delete('/:id', passport.authenticate("jwt", {session: false, failureRedirect: '/prohibido'}), (req, res) => {
    let id = req.params.id;
    Inmueble.findByIdAndRemove(id).then(resultado => {
        res.send({ok: true});
    }).catch(error => {
        res.render('listar_inmuebles', {ok: false, errorMessage: 'Error al eliminar el inmueble'});
    });
})

let esValidoInmueble = nuevoInmueble => {
    let invalido = false;
    if (!nuevoInmueble.descripcion || nuevoInmueble.descripcion.lenght < 10) {
        invalido = true;
    }
    if (!nuevoInmueble.tipo) {
        invalido = true;
    }
    if (nuevoInmueble.superficie < 25) {
        invalido = true;
    }
    if (!nuevoInmueble.precio || nuevoInmueble.precio < 10000) {
        invalido = true;
    }
    if (invalido) {
        return false;
    }
    else {
        return true;
    }
}


module.exports = router;