const mongoose = require("mongoose");
const Tipo = require("./models/tipo");

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/inmuebles");

let tipo1 = new Tipo({
  nombre: "piso"
});
tipo1.save();

let tipo2 = new Tipo({
    nombre: "bungalow"
});
tipo2.save();

let tipo3 = new Tipo({
    nombre: "atico"
  });
  tipo3.save();

  
  let tipo4 = new Tipo({
    nombre: "chalet"
  });
  tipo4.save();

  
  let tipo5 = new Tipo({
    nombre: "planta_baja"
  });
  tipo5.save();
  
  