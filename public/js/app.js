function cargar(url) {
    window.location.replace("http://localhost:8080" + url);
}

function eliminarInmueble(id)
{
    $.ajax({
        url:"/inmuebles/" + id,
        type:"DELETE",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token'));
        },
        success: function(data) {
            if (data.ok) {
                cargar('/inmuebles');
            }
            else {
                $('#contenido').html(data);
            }
        }
    });
}

function nuevoInmueble() {
    $.ajax({
        url:"/nuevo_inmueble",
        type:"GET",
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token'));
        },
        success: function(data) {
            $('#contenido').html(data);
        }
    });
}

function formInmueble() {
    var formData = new FormData(document.getElementById('formuploadajax'));
    $.ajax({
        url:"/inmuebles",
        type:"POST",
        data: formData,
        contentType: false,
        cache: false,
        processData: false,
        beforeSend: function(xhr) {
            xhr.setRequestHeader('Authorization', 'Bearer '+localStorage.getItem('token'));
        },
        success: function(data) {
            if (data.ok) {
                cargar('/inmuebles');
            }
        }
    });
}

function login()
{
    $.ajax({
        url:"/usuarios/login",
        type:"POST",
        data: JSON.stringify({login: $("#login").val(), password: $("#password").val()}),
        contentType:"application/json; charset=utf-8",
        dataType:"json",
        success: function(data) {
            console.log(data);
            if (data.errorMessage) {
                $("#loginError").css('display', 'block');
            } else {
                $("#loginError").css('display', 'none');
                alert("Token: " + data.token);
                localStorage.setItem("token", data.token);
                cargar('/inmuebles');
            }
        }
    });
}